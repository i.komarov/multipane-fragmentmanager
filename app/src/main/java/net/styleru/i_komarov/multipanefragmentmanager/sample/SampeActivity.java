package net.styleru.i_komarov.multipanefragmentmanager.sample;

import android.os.Bundle;
import android.util.Log;

import net.styleru.i_komarov.multipanefragmentmanager.R;
import net.styleru.i_komarov.multipanefragmentmanager.activity.MultiPaneActivity;
import net.styleru.i_komarov.multipanefragmentmanager.core.Config;
import net.styleru.i_komarov.multipanefragmentmanager.core.Info;

/**
 * Created by i_komarov on 02.12.16.
 */

public class SampeActivity extends MultiPaneActivity {

    @Override
    public void onBackPressed() {
        if(getFragmentManagerDecorator().getFragmentCount() >= 2) {
            getFragmentManagerDecorator().popBackStack();
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getFragmentManagerDecorator().getFragmentCount() == 0) {
            Log.d("SampleActivity", "initiating list fragment");
            getFragmentManagerDecorator().add(FragmentGeneralSample.newInstance());
        }
    }

    public void nextFragment() {
        if(getMultipaneInfo().getOrientation() == Config.Orientation.LANDSCAPE && getFragmentManagerDecorator().getFragmentCount() >= 2) {
            getFragmentManagerDecorator().add(FragmentDetailsSample.newInstance());
        } else if(getMultipaneInfo().getOrientation() == Config.Orientation.LANDSCAPE) {
            getFragmentManagerDecorator().add(FragmentGeneralSample.newInstance());
        } else if(getMultipaneInfo().getOrientation() == Config.Orientation.PORTRAIT && getFragmentManagerDecorator().getFragmentCount() >= 2) {
            getFragmentManagerDecorator().add(FragmentDetailsSample.newInstance());
        } else {
            getFragmentManagerDecorator().add(FragmentGeneralSample.newInstance());
        }
    }

    @Override
    protected int getLayoutID() {
        return R.layout.activity_sample;
    }

    @Override
    protected Info getMultipaneInfo() {
        return new Info(R.id.activity_sample_general_content_container, R.id.activity_sample_detail_content_container, getCurrentOrientation());
    }
}
