package net.styleru.i_komarov.multipanefragmentmanager.core;

import android.app.Fragment;
import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by i_komarov on 02.12.16.
 */

public class State implements Parcelable {

    private String fragmentName;
    private Fragment.SavedState fragmentState;

    public State(Parcel in) {
        fragmentName = in.readString();
        fragmentState = in.readParcelable(Fragment.SavedState.class.getClassLoader());
    }

    public State(String fragmentName, Fragment.SavedState fragmentState) {
        this.fragmentName = fragmentName;
        this.fragmentState = fragmentState;
    }

    public String getFragmentName() {
        return fragmentName;
    }

    public void setFragmentName(String fragmentName) {
        this.fragmentName = fragmentName;
    }

    public Fragment.SavedState getFragmentState() {
        return fragmentState;
    }

    public void setFragmentState(Fragment.SavedState fragmentState) {
        this.fragmentState = fragmentState;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(fragmentName);
        dest.writeParcelable(fragmentState, 0);
    }

    public static Parcelable.Creator<State> CREATOR = new Creator<State>() {
        @Override
        public State createFromParcel(Parcel in) {
            return new State(in);
        }

        @Override
        public State[] newArray(int size) {
            return new State[0];
        }
    };
}
