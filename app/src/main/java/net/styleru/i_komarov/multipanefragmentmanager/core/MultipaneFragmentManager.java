package net.styleru.i_komarov.multipanefragmentmanager.core;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import net.styleru.i_komarov.multipanefragmentmanager.R;

import java.util.LinkedList;

/**
 * Created by i_komarov on 02.12.16.
 */

public class MultipaneFragmentManager implements Parcelable {

    public static final String KEY_DUALPANE_OBJECT = "net.styleru.i_komarov.core.MultipaneFragmentManager";

    private static final String TAG = "MultipaneFragmentManager";

    private FragmentManager fragmentManager;

    private OnBackStackChangeListener listenerNull = new OnBackStackChangeListener() {
        @Override
        public void onBackStackChanged() {

        }
    };

    private OnBackStackChangeListener listener = listenerNull;

    private LinkedList<State> fragmentStateList;

    private Info info;

    private boolean onRestoreInstanceState;
    private boolean onSaveInstanceState;

    public MultipaneFragmentManager(Parcel in) {
        in.readList(fragmentStateList, LinkedList.class.getClassLoader());
        info = in.readParcelable(Info.class.getClassLoader());
        this.onRestoreInstanceState = false;
        this.onSaveInstanceState = false;
    }

    public MultipaneFragmentManager(FragmentManager fragmentManager, Info info) {
        this.fragmentManager = fragmentManager;
        this.fragmentStateList = new LinkedList<>();
        this.info = info;
        onRestoreInstanceState = true;
    }

    public void attachFragmentManager(FragmentManager fragmentManager) {
        this.fragmentManager = fragmentManager;
    }

    public void detachFragmentManager() {
        this.fragmentManager = null;
    }

    public void setOrientation(Config.Orientation orientation) {
        this.info.setOrientation(orientation);
    }

    public void add(Fragment fragment) {
        this.add(fragment, true);
        listener.onBackStackChanged();
    }

    public boolean allInLayout() {
        if(info.getOrientation() == Config.Orientation.LANDSCAPE) {
            if(fragmentManager.findFragmentById(info.getGeneralContainer()) != null && fragmentManager.findFragmentById(info.getDetailsContainer()) != null) {
                return true;
            } else {
                return false;
            }
        } else {
            if(getBackStackDepth() > 1) {
                return true;
            } else {
                return false;
            }
        }
    }

    @SuppressLint("LongLogTag")
    public synchronized void replace(Fragment fragment) {
        Log.d(TAG, "replace called, backstack was: " + fragmentStateList.size());
        if(info.getOrientation() == Config.Orientation.PORTRAIT) {
            if(fragmentManager.findFragmentById(info.getGeneralContainer()) != null) {
                fragmentManager.beginTransaction().remove(fragmentManager.findFragmentById(info.getGeneralContainer())).commit();
                fragmentManager.executePendingTransactions();
            }

            fragmentManager.beginTransaction().replace(info.getGeneralContainer(), fragment).commit();
            fragmentManager.executePendingTransactions();
        } else {
            if(fragmentManager.findFragmentById(info.getDetailsContainer()) != null) {
                fragmentManager.beginTransaction()
                        .remove(fragmentManager.findFragmentById(info.getDetailsContainer()))
                        .commit();
                fragmentManager.executePendingTransactions();
            }

            fragmentManager.beginTransaction()
                    .replace(info.getDetailsContainer(), fragment)
                    .commit();
        }
    }

    private synchronized void add(Fragment fragment, boolean addToBackStack) {
        if(info.getOrientation() == Config.Orientation.PORTRAIT) {
            if(fragmentManager.findFragmentById(info.getGeneralContainer()) != null) {
                if(addToBackStack) {
                    saveOldestVisibleFragmentState();
                }
                fragmentManager.beginTransaction().remove(fragmentManager.findFragmentById(info.getGeneralContainer())).commit();
                fragmentManager.executePendingTransactions();
            }
            fragmentManager.beginTransaction().replace(info.getGeneralContainer(), fragment).commit();
            fragmentManager.executePendingTransactions();
        } else if(fragmentManager.findFragmentById(info.getGeneralContainer()) == null) {
            fragmentManager.beginTransaction().replace(info.getGeneralContainer(), fragment).commit();
            fragmentManager.executePendingTransactions();
        } else if(fragmentManager.findFragmentById(info.getDetailsContainer()) == null) {
            fragmentManager.beginTransaction().replace(info.getDetailsContainer(), fragment).commit();
            fragmentManager.executePendingTransactions();
        } else {
            if(addToBackStack) {
                saveOldestVisibleFragmentState();
            }
            saveDetailsFragmentState();
            fragmentManager.beginTransaction()
                    .remove(fragmentManager.findFragmentById(info.getGeneralContainer()))
                    .remove(fragmentManager.findFragmentById(info.getDetailsContainer()))
                    .commit();
            fragmentManager.executePendingTransactions();

            fragmentManager.beginTransaction()
                    .replace(info.getGeneralContainer(), restoreFragment(fragmentStateList.getLast()))
                    .replace(info.getDetailsContainer(), fragment)
                    .commit();
            fragmentManager.executePendingTransactions();
            fragmentStateList.removeLast();
        }
    }

    @SuppressLint("LongLogTag")
    public void popBackStack() {
        Log.d(TAG, "popBackStack called, backstack was: " + fragmentStateList.size());
        if(info.getOrientation() == Config.Orientation.PORTRAIT) {
            //fragmentStateList.removeLast();

            fragmentManager.beginTransaction()
                    .remove(fragmentManager.findFragmentById(info.getGeneralContainer()))
                    .commit();
            fragmentManager.executePendingTransactions();

            fragmentManager.beginTransaction()
                    .replace(info.getGeneralContainer(), restoreFragment(fragmentStateList.getLast()))
                    .commit();

            fragmentStateList.removeLast();
        } else if(fragmentStateList.size() > 0) {
            //fragmentStateList.removeLast();
            saveOldestVisibleFragmentState();
            fragmentManager.beginTransaction()
                    .remove(fragmentManager.findFragmentById(info.getDetailsContainer()))
                    .remove(fragmentManager.findFragmentById(info.getGeneralContainer()))
                    .commit();
            fragmentManager.executePendingTransactions();
            fragmentManager.beginTransaction()
                    .replace(info.getGeneralContainer(), restoreFragment(fragmentStateList.get(fragmentStateList.size() - 2)))
                    .replace(info.getDetailsContainer(), restoreFragment(fragmentStateList.getLast()))
                    .commit();

            //remove the fragment that was in the details container before popbackstack was called as it is no longer accessible to user
            fragmentStateList.removeLast();
            fragmentStateList.removeLast();
        } else if(getFragmentCount() == 2) {
            fragmentManager.beginTransaction()
                    .remove(fragmentManager.findFragmentById(info.getDetailsContainer()))
                    .commit();
            fragmentManager.executePendingTransactions();
        }

        listener.onBackStackChanged();
    }

    @SuppressLint("LongLogTag")
    public void onRestoreInstanceState() {
        onSaveInstanceState = false;
        if(!onRestoreInstanceState) {
            onRestoreInstanceState = true;
            if (fragmentStateList != null) {
                if(info.getOrientation() == Config.Orientation.LANDSCAPE) {
                    if (fragmentStateList.size() > 1) {
                        fragmentManager.beginTransaction()
                                .replace(info.getGeneralContainer(), restoreFragment(fragmentStateList.get(fragmentStateList.size() - 2)))
                                .replace(info.getDetailsContainer(), restoreFragment(fragmentStateList.getLast()))
                                .commit();
                        //remove state of visible fragments
                        fragmentStateList.removeLast();
                        fragmentStateList.removeLast();
                        Log.d(TAG, "restored in landscape mode, backstack: " + fragmentStateList.size());
                    } else if (fragmentStateList.size() == 1) {
                        fragmentManager.beginTransaction()
                                .replace(info.getGeneralContainer(), restoreFragment(fragmentStateList.getLast()))
                                .commit();
                        //remove state of only visible fragment
                        fragmentStateList.removeLast();
                        Log.d(TAG, "restored in landscape mode, backstack is clear");
                    }
                } else {
                    fragmentManager.beginTransaction()
                            .replace(info.getGeneralContainer(), restoreFragment(fragmentStateList.getLast()))
                            .commit();
                    //remove state of visible fragment
                    fragmentStateList.removeLast();
                    Log.d(TAG, "restored in portrait mode, backstack: " + fragmentStateList.size());
                }
            }
        }

        fragmentManager.executePendingTransactions();
    }

    @SuppressLint("LongLogTag")
    public void onSaveInstanceState() {
        if(!onSaveInstanceState) {
            onRestoreInstanceState = false;
            onSaveInstanceState = true;
            if(info.getOrientation() == Config.Orientation.LANDSCAPE) {
                if(saveOldestVisibleFragmentState()) {
                    saveDetailsFragmentState();
                }
                Log.d(TAG, "saved state before recreating fragments in portrait, now stack is: " + fragmentStateList.size());
            } else if(info.getOrientation() == Config.Orientation.PORTRAIT) {
                saveOldestVisibleFragmentState();
                Log.d(TAG, "saved state before recreating fragments in landscape, now stack is: " + fragmentStateList.size());
            }
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            if (fragmentManager.findFragmentById(info.getGeneralContainer()) != null) {
                transaction.remove(fragmentManager.findFragmentById(info.getGeneralContainer()));
            }
            if (fragmentManager.findFragmentById(info.getDetailsContainer()) != null) {
                transaction.remove(fragmentManager.findFragmentById(info.getDetailsContainer()));
            }
            transaction.commit();
        }
    }

    public int getBackStackDepth() {
        return fragmentStateList.size();
    }

    public int getFragmentCount() {
        int count = 0;
        if(fragmentManager.findFragmentById(info.getGeneralContainer()) != null) {
            count++;
            if(info.getOrientation() == Config.Orientation.LANDSCAPE && fragmentManager.findFragmentById(info.getDetailsContainer()) != null) {
                count++;
            }

            count += getBackStackDepth();
        }

        return count;
    }

    private Fragment restoreFragment(State state) {
        try {
            Fragment fragment = ((Fragment) Class.forName(state.getFragmentName()).newInstance());
            fragment.setInitialSavedState(state.getFragmentState());
            return fragment;
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        return null;
    }

    @SuppressLint("LongLogTag")
    private boolean saveOldestVisibleFragmentState() {
        Fragment current = fragmentManager.findFragmentById(info.getGeneralContainer());
        if (current != null) {
            Log.d(TAG, "saveOldestVisibleFragmentState called, current was not null");
            fragmentStateList.add(new State(current.getClass().getCanonicalName(), fragmentManager.saveFragmentInstanceState(current)));
        }

        return current != null;
    }

    @SuppressLint("LongLogTag")
    private boolean saveDetailsFragmentState() {
        Fragment details = fragmentManager.findFragmentById(info.getDetailsContainer());
        if(details != null) {
            Log.d(TAG, "saveDetailsFragmentState called, details was not null");
            fragmentStateList.add(new State(details.getClass().getCanonicalName(), fragmentManager.saveFragmentInstanceState(details)));
        }

        return details != null;
    }

    public void setOnBackStackChangeListener(OnBackStackChangeListener listener) {
        this.listener = listener;
    }

    public void removeOnBackStackChangeListener() {
        this.listener = listenerNull;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(fragmentStateList);
        dest.writeParcelable(info, 0);
    }

    public static Parcelable.Creator<MultipaneFragmentManager> CREATOR = new Creator<MultipaneFragmentManager>() {
        @Override
        public MultipaneFragmentManager createFromParcel(Parcel in) {
            return new MultipaneFragmentManager(in);
        }

        @Override
        public MultipaneFragmentManager[] newArray(int size) {
            return new MultipaneFragmentManager[0];
        }
    };
}
