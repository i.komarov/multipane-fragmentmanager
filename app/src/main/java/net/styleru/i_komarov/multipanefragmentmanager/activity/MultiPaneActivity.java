package net.styleru.i_komarov.multipanefragmentmanager.activity;

import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import net.styleru.i_komarov.multipanefragmentmanager.core.Config;
import net.styleru.i_komarov.multipanefragmentmanager.core.Info;
import net.styleru.i_komarov.multipanefragmentmanager.core.MultipaneFragmentManager;
import net.styleru.i_komarov.multipanefragmentmanager.core.OnBackStackChangeListener;

/**
 * Created by i_komarov on 02.12.16.
 */

public abstract class MultiPaneActivity extends AppCompatActivity implements OnBackStackChangeListener {

    private MultipaneFragmentManager fragmentManager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutID());
        if(savedInstanceState != null) {
            fragmentManager = savedInstanceState.getParcelable(MultipaneFragmentManager.KEY_DUALPANE_OBJECT);
            fragmentManager.setOrientation(getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE ? Config.Orientation.LANDSCAPE : Config.Orientation.PORTRAIT);
            fragmentManager.attachFragmentManager(getFragmentManager());
            fragmentManager.setOnBackStackChangeListener(this);
            fragmentManager.onRestoreInstanceState();
        }
        if(fragmentManager == null) {
            fragmentManager = new MultipaneFragmentManager(getFragmentManager(), getMultipaneInfo());
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        fragmentManager.onSaveInstanceState();
        fragmentManager.removeOnBackStackChangeListener();
        fragmentManager.detachFragmentManager();
        super.onSaveInstanceState(outState);
        outState.putParcelable(MultipaneFragmentManager.KEY_DUALPANE_OBJECT, fragmentManager);
        fragmentManager = null;
    }

    @Override
    public void onBackStackChanged() {

    }

    protected MultipaneFragmentManager getFragmentManagerDecorator() {
        return fragmentManager;
    }

    protected Config.Orientation getCurrentOrientation() {
        return getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE ? Config.Orientation.LANDSCAPE : Config.Orientation.PORTRAIT;
    }

    protected abstract int getLayoutID();

    protected abstract Info getMultipaneInfo();
}
