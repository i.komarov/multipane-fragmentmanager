package net.styleru.i_komarov.multipanefragmentmanager.core;

/**
 * Created by i_komarov on 02.12.16.
 */

public class Config {

    public enum Orientation {

        LANDSCAPE,
        PORTRAIT
    }
}
