package net.styleru.i_komarov.multipanefragmentmanager.core;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.IdRes;

/**
 * Created by i_komarov on 02.12.16.
 */

public class Info implements Parcelable {

    private static final byte ORIENTATION_LANDSCAPE = 0;
    private static final byte ORIENTATION_PORTRAIT = 1;

    @IdRes
    private int generalContainer;
    @IdRes
    private int detailsContainer;

    private Config.Orientation orientation;

    public Info(Parcel in) {
        this.generalContainer = in.readInt();
        this.detailsContainer = in.readInt();
        this.orientation = in.readByte() == ORIENTATION_LANDSCAPE ? Config.Orientation.LANDSCAPE : Config.Orientation.PORTRAIT;
    }

    public Info(int generalContainer, int detailsContainer, Config.Orientation orientation) {
        this.generalContainer = generalContainer;
        this.detailsContainer = detailsContainer;
        this.orientation = orientation;
    }

    public int getGeneralContainer() {
        return generalContainer;
    }

    public void setGeneralContainer(int generalConteiner) {
        this.generalContainer = generalConteiner;
    }

    public int getDetailsContainer() {
        return detailsContainer;
    }

    public void setDetailsContainer(int detailsContainer) {
        this.detailsContainer = detailsContainer;
    }

    public Config.Orientation getOrientation() {
        return orientation;
    }

    public void setOrientation(Config.Orientation orientation) {
        this.orientation = orientation;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(generalContainer);
        dest.writeInt(detailsContainer);
        dest.writeByte(orientation == Config.Orientation.LANDSCAPE ? ORIENTATION_LANDSCAPE : ORIENTATION_PORTRAIT);
    }

    public static Parcelable.Creator<Info> CREATOR = new Creator<Info>() {
        @Override
        public Info createFromParcel(Parcel in) {
            return new Info(in);
        }

        @Override
        public Info[] newArray(int size) {
            return new Info[0];
        }
    };
}
