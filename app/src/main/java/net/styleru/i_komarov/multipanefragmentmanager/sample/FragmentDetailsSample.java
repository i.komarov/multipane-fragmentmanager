package net.styleru.i_komarov.multipanefragmentmanager.sample;

import android.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import net.styleru.i_komarov.multipanefragmentmanager.R;

/**
 * Created by i_komarov on 02.12.16.
 */

public class FragmentDetailsSample extends Fragment {

    private AppCompatTextView text;

    public static FragmentDetailsSample newInstance() {
        return new FragmentDetailsSample();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_details_sample, parent, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        text = (AppCompatTextView) view.findViewById(R.id.fragment_details_sample_item);
        text.setText("Detailed item representation");
    }
}
