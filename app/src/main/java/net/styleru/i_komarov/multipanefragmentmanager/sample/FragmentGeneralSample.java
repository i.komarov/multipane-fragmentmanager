package net.styleru.i_komarov.multipanefragmentmanager.sample;

import android.app.Fragment;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import net.styleru.i_komarov.multipanefragmentmanager.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by i_komarov on 02.12.16.
 */

public class FragmentGeneralSample extends Fragment {

    private static final String STATE_LIST = "net.styleru.i_komarov.multipanefragmentmanager.sample.FragmentGeneralSample.STATE_LIST";
    private static final String STATE_ADAPTER = "net.styleru.i_komarov.multipanefragmentmanager.sample.FragmentGeneralSample.STATE_ADAPTER";
    private static final String STATE_LIST_POSITION = "net.styleru.i_komarov.multipanefragmentmanager.sample.FragmentGeneralSample.STATE_LIST_POSITION";

    private View root;

    private RecyclerView list;

    private SimpleAdapter adapter;

    private Parcelable listState;

    public static FragmentGeneralSample newInstance() {
        return new FragmentGeneralSample();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_general_sample, parent, false);
        return root;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        list = (RecyclerView) view.findViewById(R.id.fragment_general_sample_list);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if(listState != null) {
            outState.putParcelable(STATE_LIST, listState);
        } else if(list != null && list.getLayoutManager() != null) {
            outState.putParcelable(STATE_LIST, list.getLayoutManager().onSaveInstanceState());
        }
        /*if(list != null && list.getLayoutManager() != null) {
            outState.putInt(STATE_LIST_POSITION, ((LinearLayoutManager) list.getLayoutManager()).findFirstCompletelyVisibleItemPosition());
        }*/
        if(adapter != null) {
            outState.putParcelable(STATE_ADAPTER, adapter);
        }
    }

    @Override
    public void onViewStateRestored(Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        Log.d("FragmentGeneralSample", "onViewStateRestored called, savedInstanceState: " + (savedInstanceState == null ? "null" : "not null"));
        if(savedInstanceState != null && savedInstanceState.containsKey(STATE_LIST)) {
            listState = savedInstanceState.getParcelable(STATE_LIST);
            Log.d("FragmentGeneralSample", "list state restored from parcelable, is null ? : " + (listState == null));
        }
        if(savedInstanceState != null && savedInstanceState.containsKey(STATE_ADAPTER)) {
            Log.d("FragmentGeneralSample", "adapter restored from parcelable");
            adapter = savedInstanceState.getParcelable(STATE_ADAPTER);
        }
        if(adapter == null) {
            Log.d("FragmentGeneralSample", "adapter is null and thus would be created");
            adapter = new SimpleAdapter(mockItems());
        }
        if(list.getAdapter() == null) {
            list.setLayoutManager(new LinearLayoutManager(root.getContext()));
            adapter.setOnItemClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((SampeActivity) getActivity()).nextFragment();
                }
            });
            list.setAdapter(adapter);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d("FragmentGeneralSample", "onResume");
        if(listState != null) {
            Log.d("FragmentGeneralSample", "onResume, list state is not null");
            list.getLayoutManager().onRestoreInstanceState(listState);
        }
    }

    @Override
    public void onPause() {
        listState = list.getLayoutManager().onSaveInstanceState();
        list.setAdapter(null);
        adapter.setOnItemClickListener(null);
        super.onPause();
    }

    private List<String> mockItems() {
        List<String> items = new ArrayList<>();
        for(int i = 0; i < 20; i++) {
            items.add("Элемент #" + i);
        }

        return items;
    }

    private static class SimpleVH extends RecyclerView.ViewHolder {

        private AppCompatTextView textHolder;

        public SimpleVH(View itemView) {
            super(itemView);
            textHolder = (AppCompatTextView) itemView.findViewById(R.id.list_item_general_fragment_text);

        }

        public void bind(String text, View.OnClickListener listener) {
            textHolder.setText(text);
            itemView.setOnClickListener(listener);
        }
    }

    private static class SimpleAdapter extends RecyclerView.Adapter<SimpleVH> implements Parcelable {

        private List<String> items;

        private View.OnClickListener listenerNull = new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        };

        private View.OnClickListener listener = listenerNull;

        public SimpleAdapter(Parcel in) {
            in.readStringList(items);
        }

        public SimpleAdapter(List<String> items) {
            this.items = items;
        }

        @Override
        public SimpleVH onCreateViewHolder(ViewGroup parent, int viewType) {
            return new SimpleVH(LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_general_fragment, parent, false));
        }

        @Override
        public void onBindViewHolder(SimpleVH holder, int position) {
            holder.bind(items.get(position), listener);
        }

        @Override
        public int getItemCount() {
            return items.size();
        }

        public void setOnItemClickListener(View.OnClickListener listener) {
            if(listener != null) {
                this.listener = listener;
            } else {
                this.setOnItemClickListener(listenerNull);
            }
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeStringList(items);
        }

        public static Parcelable.Creator<SimpleAdapter> CREATOR = new Creator<SimpleAdapter>() {
            @Override
            public SimpleAdapter createFromParcel(Parcel in) {
                return new SimpleAdapter(in);
            }

            @Override
            public SimpleAdapter[] newArray(int size) {
                return new SimpleAdapter[0];
            }
        };
    }
}
